<?php 
	include_once '../model/Conexao.class.php';
	include_once '../model/Gerenciamento.class.php';

	$gerenciamento = new Gerenciamento();

	$id = $_POST['id'];
	if(isset($id) && !empty($id)){
		$gerenciamento->deletar_cliente("equipamento", $id);
		header("Location: ../index.php?cliente_deletado_com_sucesso");

	}
 ?>