<?php 

	class Gerenciamento extends Conexao{

		public function inserir_clientes($tabela, $data){
			$pdo = parent::get_instance();
			$fields = implode(",", array_keys($data));
			$values = ":".implode(", :", array_keys($data));
			$sql = "INSERT INTO $tabela ($fields) VALUES ($values)";
			$stmt = $pdo->prepare($sql);
			foreach ($data as $key => $value) {
				$stmt->bindValue(":$key", $value, PDO::PARAM_STR);
			}
			$stmt->execute();
		}

		public function lista_clientes($tabela){
			$pdo = parent::get_instance();
			$sql = "SELECT * FROM $tabela";
			$stmt = $pdo->query($sql);
			$stmt->execute();
			return $stmt->fetchAll();	
		}

		public function deletar_cliente($tabela, $id){
			$pdo = parent::get_instance();
			$sql = "DELETE FROM $tabela WHERE EQUIPAMENTO_ID = :id";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(":id", $id);
			$stmt->execute();
		}
	}
 ?>