<?php 
	class Conexao{
		public static $instance;

		//função responsável por iniciar a conexão com o BD
		public static function get_instance(){
			//Se não existe a variavél de instancia, cria a variavel
			if(!isset(self::$instance)){
				self::$instance = new PDO("mysql:host=localhost;dbname=equipamentos;", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			return self::$instance;

				
		}
	}


 ?>