<!DOCTYPE html>
<html>
<head>
	<?php include_once 'dependencias.php'; ?>
	
</head>
<body>
	<h2 class="text-center">
		Criar Equipamento<i class="fa fa-user-plus"></i>
	</h2><hr>
	<form method="POST" action="../controller/inserir_cliente.php">
		<div class="container">
			<div class="form-row">
				<div class="col-md-7">
					Codigo: <i class="fas fa-barcode"></i>
					<input type="text" name="codigo" required autofocus class="form-control"><br>
				</div>
				<div class="col-md-6">
					Nome: <i class="fa fa-user"></i>
					<input type="text" name="equi" required class="form-control"><br>
				</div>
				<div class="col-md-7">
					Descrição: <i class="fa fa-audio-description"></i>
					<input type="text" name="desc" required class="form-control"><br>
				</div>

				<div class="col-md-10">
					<button class="btn btn-primary btn-md">Criar Registro <i class="fa fa-user-plus"></i></button>
					<a href="../index.php">Voltar</a>
				</div>

			</div>

		</div>
	</form>	

</body>
</html>