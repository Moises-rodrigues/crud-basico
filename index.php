<!DOCTYPE html>
<html>
<head>
	<?php 
		  include_once 'view/dependencias.php';
		  include_once 'model/Conexao.class.php';
		  include_once 'model/Gerenciamento.class.php';

		  $gerenciamento = new Gerenciamento();
	?>
</head>
<body>

<div class="container">
	
	<h2 class="text-center"> Lista de Equipamentos <i class="fa fa-users"></i></h2>

	<h5 class="text-right">
		<a href="view/criar_registro.php" class="btn btn-primary btn-xs">
			<i class="fa fa-user-plus"></i>
		</a>
	</h5>
	<div class="table-responsive">
		
		<table class="table table-hover">
			<thead class="thead">
				<tr>
					<th>Codigo</th>
					<th>Nome</th>
					<th>Descrição</th>
					<th>Outro TH</th>
                    <th></th>
			</thead>
			<tbody>
				<?php foreach($gerenciamento->lista_clientes("equipamento") as $equipamento): ?>
				<tr>
					<td><?php echo $equipamento['codigo']; ?></td>
					<td><?php echo $equipamento['nome']; ?></td>
					<td><?php echo $equipamento['descricao']; ?></td>
					<td>tome-lhe</td>
					<td>
						<form method="POST">
							<button class="btn btn-warning btn-xs">
								<i class="fa fa-user-edit"></i>
							</button>
						</form>
					</td>
					<td>
						<form method="POST" action="controller/deletar_cliente.php" onclick="return confirm('Tem certeza que deseja excluir ?');">
							<input type="hidden" name="id" value="<?=$equipamento['EQUIPAMENTO_ID']?>">
							<button class="btn btn-danger btn-xs">
								<i class="fa fa-trash"></i>
							</button>
						</form>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>